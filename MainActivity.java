//copy of original file
package com.example.w8;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    TextView StatusText;
    EditText BottleNumber;
    SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StatusText = (TextView) findViewById(R.id.StatusText1);
        BottleNumber = (EditText)findViewById(R.id.editTextNumber);
        seekBar=(SeekBar)findViewById(R.id.seekBar);

        StatusText.setText("Bottle Dispenser App 1.0!");
        //suspect
        BottleDispenser bd = BottleDispenser.getInstance();

    }

    public void buttonlist(View v) {
        StatusText.setText("Money: " + BottleDispenser.fetchMoney() + "\n" + BottleDispenser.listBottles());
    }

    public void buttonBuy(View v) {
        String strValue = BottleNumber.getText().toString();
        int valinta = Integer.parseInt(strValue);
        StatusText.setText("Money: " + BottleDispenser.fetchMoney() + "\n"+BottleDispenser.buyBottle(valinta));

    }
    public void buttonAddMoney(View v){
        float value = seekBar.getProgress();
        value = (float) (value / 20);
        StatusText.setText(BottleDispenser.addMoney(value)+ "\n"+"\n"+"Money: " + BottleDispenser.fetchMoney() );
        seekBar.setProgress(0);
    }
    public void buttonRemoveMoney(View v){
        StatusText.setText(BottleDispenser.returnMoney());
    }

}