package com.example.w8;
import android.view.View;

import java.util.Scanner;
import java.util.ArrayList;

public class BottleDispenser {

    private static int bottles;
    static ArrayList<Bottle> bottleList = new ArrayList<Bottle>();
    private static double money;

    private static BottleDispenser BD = new BottleDispenser();

    private BottleDispenser() {
        bottles = 5;
        money = 0.0;
        // Add Bottle-objects to the array
        bottleList.add(new Bottle("Pepsi Max", "Pepsi", 1.8, 0.5));
        bottleList.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 2.0, 0.5));
        bottleList.add(new Bottle("1L of Gasoline", "Neste", 2.2, 1.0));
        bottleList.add(new Bottle("Sprite Cranberry", "Coca-Cola", 2.5, 1.5));
        bottleList.add(new Bottle("Fanta Zero", "Fanta", 1.95, 0.5));
    }

    public static BottleDispenser getInstance(){
        return BD;
    }


    public static String addMoney(float value) {
        money += value;
        System.out.println("Klink! Added more money: "+value);
        String s = ("Klink! Added more money: "+value);
        return s;
    }
    public static String fetchMoney(){
        String s = String.format("%.2f", money);
        return s;
    }
    public static String buyBottle(int input) {
        String s="";
        input-=1;
        if(bottles>0 && money>bottleList.get(input).getPrice()){
            bottles -= 1;
            money-=bottleList.get(input).getPrice();
            System.out.println("KACHUNK! "+bottleList.get(input).getName()+" came out of the dispenser!");
            s=("KACHUNK! "+bottleList.get(input).getName()+" came out of the dispenser!");
            //new
            bottleList.remove(input);
            return s;
        }
        else{
            System.out.println("Add money first!");
            s=("Add money first!");
            return s;
        }
    }

    public static String returnMoney() {

        System.out.println("Klink klink. Money came out! You got "+String.format("%.2f", money)+"€ back");
        String s = ("Klink klink. Money came out! You got "+String.format("%.2f", money)+"€ back");
        money = 0;
        return s;
    }
    public static String listBottles() {
        int i=0;
        String s = "";
        //print bottles

        try {
            while (i < 5) {
                System.out.println(i + 1 + ". Name: " + bottleList.get(i).getName()+"\n");
                s= s+(i + 1 + ". Name: " + bottleList.get(i).getName()+"\n");
                System.out.println("	Size: " + bottleList.get(i).getSize() + "	Price: " + bottleList.get(i).getPrice()+"\n");
                s = s+("	Size: " + bottleList.get(i).getSize() + "	Price: " + bottleList.get(i).getPrice()+"\n");
                i++;
            }
        }catch(Exception e) {
                System.out.println("oof...");
        }
        return s;
    }
}
