package com.example.w8;
/**
 *
 * @author jaakk
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;
    public Bottle(){
        name="Pepsi Max";
        manufacturer="Pepsi";
        total_energy=0.3;
        size=0.5;
        price=1.8;
    }
    public Bottle(String n, String manuf, double Pr, double Sz){
        name=n;
        manufacturer=manuf;
        price=Pr;
        size=Sz;

    }
    public String getName(){
        return name;
    }
    public void setPrice(double newP){
        price=newP;
    }
    public void setSize(double newP){
        size=newP;
    }
    public double getPrice(){
        return price;
    }
    public double getSize(){
        return size;
    }
    public String getManufacturer(){
        return manufacturer;

    }
    public double getEnergy(){
        return total_energy;
    }
}